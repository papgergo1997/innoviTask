import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Item } from 'src/app/model/item';
import { ItemService } from 'src/app/service/item.service';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class DeleteDialogComponent implements OnInit {
  data: any;

  constructor(
    private itemService: ItemService,
    private storageService: StorageService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.data = data;
  }

  ngOnInit(): void {}

  close() {
    this.dialogRef.close();
  }
  //késsőbb még lehet rossz lesz !!!
  delete(el: any): void {
    if (el.address) {
      this.storageService.delete(el);
      this.snackBar.open('Sikeres Törlés', 'Bezárás', {
        duration: 3000,
      });
      this.close();
    } else {
      this.itemService.delete(el);
      this.snackBar.open('Sikeres Törlés', 'Bezárás', {
        duration: 3000,
      });
      this.close();
    }
  }
}
