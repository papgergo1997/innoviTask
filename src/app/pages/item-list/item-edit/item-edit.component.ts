import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/model/user';
import { ItemService } from 'src/app/service/item.service';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.scss'],
})
export class ItemEditComponent implements OnInit {
  currentUser: User;
  selectOpt: number[] = [1, 2, 3, 4, 5];

  form = new FormGroup({
    id: new FormControl(''),
    name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    length: new FormControl('', [
      Validators.required,
      Validators.pattern('[1-5]'),
    ]),
    width: new FormControl('', [
      Validators.required,
      Validators.pattern('[1-5]'),
    ]),
    added: new FormControl(''),
  });

  constructor(
    private itemService: ItemService,
    private dialogRef: MatDialogRef<ItemEditComponent>,
    private snackBar: MatSnackBar,
    private date: DatePipe,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.form.patchValue(data);
  }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user') || 'null');
  }

  onSave() {
    if (this.currentUser.perm == 'ADMIN') {
      this.save();
      this.snackBar.open('Sikeres Mentés', 'Bezárás', {
        duration: 3000,
      });
    } else if (this.currentUser.perm == 'USER') {
      this.snackBar.open('Hozzáférés Megtagadva', 'Bezárás', {
        duration: 3000,
      });
    }
  }
  save() {
    if (this.form.get('id')?.value != 0) {
      this.itemService.update(this.form.value);
      this.dialogRef.close();
    } else {
      this.form
        .get('added')
        ?.setValue(this.date.transform(new Date(), 'yyyy.MM.dd hh:mm a'));
      this.itemService.create(this.form.value);
      this.dialogRef.close();
    }
  }

  close() {
    this.dialogRef.close();
  }
}
