import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { Item } from 'src/app/model/item';
import { ItemService } from 'src/app/service/item.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ItemEditComponent } from './item-edit/item-edit.component';
import { User } from 'src/app/model/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeleteDialogComponent } from '../pageComponents/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit, OnDestroy {
  list$: Observable<Item[]> = new Observable<Item[]>();
  item: Item = { id: '0', name: '', width: 0, length: 0, added: '' };
  subscription: Subscription = new Subscription();
  currentUser: User;

  @ViewChild('paginator') paginator: MatPaginator;

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  colDef: Array<any> = [
    { key: 'name', header: 'Tárgy neve' },
    { key: 'length', header: 'Tárgy hossza' },
    { key: 'added', header: 'Létrehozás dátuma' },
    { key: 'edit', header: '' },
  ];
  columns: string[] = ['name', 'length', 'added', 'edit'];

  constructor(
    private itemService: ItemService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.list$ = this.itemService.list$;
    this.subscription = this.itemService.list$.subscribe((list) => {
      this.dataSource = new MatTableDataSource(list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = function (record, filter) {
        return record.name.toLowerCase().includes(filter.toLocaleLowerCase());
      };
    });
    this.currentUser = JSON.parse(localStorage.getItem('user') || 'null');

    this.itemService.getAll();
  }

  openDialog(el: any, comp: 'del' | 'edit'): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = el;
    if (comp == 'del') {
      this.dialog.open(DeleteDialogComponent, dialogConfig);
    } else {
      this.dialog.open(ItemEditComponent, dialogConfig);
    }
  }
  onDelete(el: Item) {
    if (this.currentUser.perm == 'ADMIN') {
      this.openDialog(el, 'del');
    } else if (this.currentUser.perm == 'USER') {
      this.snackBar.open('Hozzáférés Megtagadva', 'Bezárás', {
        duration: 3000,
      });
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
