import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/model/user';
import { StorageService } from 'src/app/service/storage.service';

@Component({
  selector: 'app-storage-edit',
  templateUrl: './storage-edit.component.html',
  styleUrls: ['./storage-edit.component.scss'],
})
export class StorageEditComponent implements OnInit {
  currentUser: User;

  selectOpt: number[] = [1, 2, 3, 4, 5];

  form = new FormGroup({
    id: new FormControl({ value: '', disabled: true }, Validators.required),
    address: new FormControl('', [
      Validators.required,
      Validators.maxLength(50),
    ]),
    width: new FormControl('', [
      Validators.required,
      Validators.pattern('[1-5]'),
    ]),
    length: new FormControl('', [
      Validators.required,
      Validators.pattern('[1-5]'),
    ]),
  });
  constructor(
    private stService: StorageService,
    private dialogRef: MatDialogRef<StorageEditComponent>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.form.patchValue(data);
  }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user') || 'null');
  }

  onSave() {
    if (this.currentUser.perm == 'ADMIN') {
      this.save();
      this.snackBar.open('Sikeres Mentés', 'Bezárás', {
        duration: 3000,
      });
    } else if (this.currentUser.perm == 'USER') {
      this.snackBar.open('Hozzáférés Megtagadva', 'Bezárás', {
        duration: 3000,
      });
    }
  }
  save() {
    if (this.form.get('id')?.value == '0') {
      this.form.get('id').setValue(
        Math.random()
          .toString(36)
          .replace(/[^a-zA-Z0-9]+/g, '')
          .substr(2, 10)
      );
      this.stService.create(this.form.getRawValue());
      this.dialogRef.close();
    } else {
      this.stService.update(this.form.getRawValue());
      this.dialogRef.close();
    }
  }
  close() {
    this.dialogRef.close();
  }
}
