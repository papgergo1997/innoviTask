import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Storage } from 'src/app/model/storage';
import { StorageService } from 'src/app/service/storage.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { User } from 'src/app/model/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeleteDialogComponent } from '../pageComponents/delete-dialog/delete-dialog.component';
import { StorageEditComponent } from './storage-edit/storage-edit.component';
import { ItemService } from 'src/app/service/item.service';

@Component({
  selector: 'app-storage-list',
  templateUrl: './storage-list.component.html',
  styleUrls: ['./storage-list.component.scss'],
})
export class StorageListComponent implements OnInit, OnDestroy {
  list$: Observable<Storage[]>;
  storage: Storage = { id: '0', address: '', width: 0, length: 0 };
  datasourceSubscription: Subscription = new Subscription();
  itemSpaceSub: Subscription = new Subscription();
  stSpaceSub: Subscription = new Subscription();
  currentUser: User;
  itemSpace: number = 0;
  stSpace: number = 0;

  @ViewChild('paginator') paginator: MatPaginator;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  colDef: Array<any> = [
    { key: 'id', header: 'Azonosító' },
    { key: 'address', header: 'Cím' },
    { key: 'length', header: 'Hosszúság' },
    { key: 'width', header: 'Szélesség' },
    { key: 'edit', header: '' },
  ];
  columns: string[] = ['id', 'address', 'length', 'width', 'edit'];

  constructor(
    private itService: ItemService,
    private stService: StorageService,

    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.list$ = this.stService.list$;
    this.datasourceSubscription = this.stService.list$.subscribe((list) => {
      this.dataSource = new MatTableDataSource(list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = function (record, filter) {
        return record.address
          .toLowerCase()
          .includes(filter.toLocaleLowerCase());
      };
    });
    this.currentUser = JSON.parse(localStorage.getItem('user') || 'null');
    this.stService.getAll();
    this.getSpaces();
  }

  openDialog(el: any, comp: 'del' | 'edit'): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = el;
    if (comp == 'del') {
      this.dialog.open(DeleteDialogComponent, dialogConfig);
    } else {
      this.dialog.open(StorageEditComponent, dialogConfig);
    }
  }
  onDelete(el: Storage) {
    if (this.currentUser.perm == 'ADMIN') {
      this.openDialog(el, 'del');
    } else if (this.currentUser.perm == 'USER') {
      this.snackBar.open('Hozzáférés Megtagadva', 'Bezárás', {
        duration: 3000,
      });
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getSpaces() {
    this.stSpaceSub = this.stService.list$.subscribe((list) => {
      this.stSpace = 0;
      list.map((item) => (this.stSpace += item.width * item.length));
    });
    this.itemSpaceSub = this.itService.list$.subscribe((list) =>
      list.map((item) => (this.itemSpace += item.width * item.length))
    );
  }

  ngOnDestroy(): void {
    this.datasourceSubscription.unsubscribe();
    this.itemSpaceSub.unsubscribe();
    this.stSpaceSub.unsubscribe();
  }
}
