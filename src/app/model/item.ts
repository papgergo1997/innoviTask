export interface Item {
  id: string;
  name: string;
  length: number;
  width: number;
  added: string;
}
