export interface Storage {
  id: string;
  address: string;
  width: number;
  length: number;
}
