import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  users: any;
  currentUser: any;

  constructor(private userService: UserService, private router: Router) {
    this.users = of(this.userService.users);
  }

  auth(email: string, password: string): void {
    this.users.subscribe((users) =>
      users.map((user) => {
        if (user.email === email && user.password === password) {
          this.currentUser = user;
          localStorage.setItem('user', JSON.stringify(this.currentUser));
          JSON.parse(localStorage.getItem('user') || 'null');
          this.router.navigate(['items']);
        } else {
          return;
        }
      })
    );
  }
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user') || 'null');
    return user !== null ? true : false;
  }
}
