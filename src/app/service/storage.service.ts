import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '../model/storage';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class StorageService extends BaseService<Storage> {
  constructor(public http: HttpClient) {
    super(http, 'storages');
  }
}
