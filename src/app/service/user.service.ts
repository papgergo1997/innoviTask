import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService<User> {
  users = [
    {
      id: 1,
      name: 'John Doe',
      email: 'john.doe@gmail.com',
      password: 'johndoe98',
      perm: 'USER',
    },
    {
      id: 2,
      name: 'Jane Doe',
      email: 'jane.doe@gmail.com',
      password: 'janedoe100',
      perm: 'ADMIN',
    },
  ];
  constructor(public http: HttpClient) {
    super(http, 'users');
  }
}
